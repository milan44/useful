# Bugs/Errors/etc.

Please create a detailed Issue with the Bug/Error/etc. that you`ve found.

# Index

 * [Snippets](https://gitlab.com/milan44/useful/snippets)
 * Other (Sorted: old to new)
     * [error.php](https://gitlab.com/milan44/useful/error.php/)
     * [draggable.js](https://gitlab.com/milan44/useful/tree/master/draggable.js)  