Object.defineProperty(HTMLMediaElement.prototype, 'playing', {
    get: function(){
        return !!(this.currentTime > 0 && !this.paused && !this.ended && this.readyState > 2);
    }
});

(function() {
    var player = function(video, player, ambilight) {
        this.vidobj = null;
        this.html = null;
        this.light = ambilight;
        this.player = player;
        this.vidurl = video;
        this.timeout = null;
        this.create = function() {
            this.player = document.querySelector(this.player);
            
            var obj = document.createElement("video");
            obj.src = this.vidurl;
            obj.controls = false;
            
            obj.addEventListener('ended',this.ended,false);
            
            obj.setAttribute("class", "video object");
            
            obj.addEventListener("click", this.video, false);
            
            var vid = this.video;
            
            obj.onkeyup = function(e){
                if(e.keyCode == 32){
                    vid(null, obj);
                }
            }
            
            var html = getHTML(obj);
            this.player.appendChild(html.player);
            
            if (this.light)
                this.ambilight(obj);
            
            html.play.onclick = this.play;
            html.pause.onclick = this.pause;
            html.restart.onclick = this.restart;
            
            html.mute.onclick = this.mute;
            html.unmute.onclick = this.unmute;
            
            this.vidobj = obj;
            this.html = html;
            
            var parent = obj.parentElement;

            canvas = parent.getElementsByClassName('video-canvas')[0];
            ctx = canvas.getContext('2d');

            ctx.drawImage(obj, 0, 0, 600, 460);
            
            this.update(this);
        };
        
        this.ambilight = function (videoobj) {
            var timerID;
            var parent = videoobj.parentElement;

            canvas = parent.getElementsByClassName('video-canvas')[0];
            ctx = canvas.getContext('2d');

            var video = videoobj;

            video.addEventListener('play', function() {
                timerID = window.setInterval(function() {
                    ctx.drawImage(video, 0, 0, 600, 460);
                }, 100);
            });

            video.addEventListener('pause', function() {
                clearInterval(timerID);
                ctx.drawImage(video, 0, 0, 600, 460);
            });

            video.addEventListener('ended', function() {
                clearInterval(timerID);
                ctx.drawImage(video, 0, 0, 600, 460);
            });
        };
        
        this.video = function(asdf, _this) {
            if (_this == null)
                _this = this;
            if (_this.playing) {
                var player = _this.parentElement.parentElement;
                var pause = player.querySelector("button.pause.button");
                pause.click();
            } else {
                var player = _this.parentElement.parentElement;
                var play = player.querySelector("button.play.button");
                play.click();
            }
        };
        
        this.mute = function() {
            var player = this.parentElement.parentElement;
            var mute = this.parentElement.querySelector("button.mute.button");
            var unmute = this.parentElement.querySelector("button.unmute.button");
            var vidobj = player.querySelector("video.video.object");
            
            vidobj.muted = true;
            
            mute.classList.add("hidden");
            unmute.classList.remove("hidden");
        };
        this.unmute = function() {
            var player = this.parentElement.parentElement;
            var mute = this.parentElement.querySelector("button.mute.button");
            var unmute = this.parentElement.querySelector("button.unmute.button");
            var vidobj = player.querySelector("video.video.object");
            
            vidobj.muted = false;
            
            mute.classList.remove("hidden");
            unmute.classList.add("hidden");
        };
        
        this.restart = function() {
            var player = this.parentElement.parentElement;
            var pause = this.parentElement.querySelector("button.pause.button");
            var play = this.parentElement.querySelector("button.play.button");
            var vidobj = player.querySelector("video.video.object");
            
            vidobj.currentTime = 0;
            vidobj.play();
            
            this.classList.add("hidden");
            pause.classList.remove("hidden");
            play.classList.add("hidden");
        };
        this.play = function() {
            var player = this.parentElement.parentElement;
            var pause = this.parentElement.querySelector("button.pause.button");
            var vidobj = player.querySelector("video.video.object");
            
            vidobj.pause();
            
            vidobj.play();
            this.classList.add("hidden");
            pause.classList.remove("hidden");
        };
        this.pause = function() {
            var player = this.parentElement.parentElement;
            var play = this.parentElement.querySelector("button.play.button");
            var vidobj = player.querySelector("video.video.object");
            vidobj.pause();
            this.classList.add("hidden");
            play.classList.remove("hidden");
        };
        this.update = function(_this) {
            if (_this.vidobj.playing) {
                _this.updateProgressBar();
            }
            
            _this.timeout = setTimeout(_this.update, 100, _this);
        };
        this.getProgress = function() {
            var totalLength = this.vidobj.duration;
            var playedLength = this.vidobj.currentTime;
            
            var percentage = (playedLength / totalLength) * 100;
            return percentage;
        };
        this.updateProgressBar = function() {
            this.html.progress.style.width = this.getProgress() + "%";
        };
        this.ended = function() {
            var player = this.parentElement.parentElement;
            var play = player.querySelector("button.play.button");
            var restart = player.querySelector("button.restart.button");
            var pause = player.querySelector("button.pause.button");
            var vidobj = player.querySelector("video.video.object");
            pause.classList.add("hidden");
            play.classList.add("hidden");
            restart.classList.remove("hidden");
        };
    };
    
    function getHTML(obj) {
        var play = document.createElement("button");
        play.setAttribute("class", "play button");

        var pause = document.createElement("button");
        pause.setAttribute("class", "pause button hidden");
        
        var restart = document.createElement("button");
        restart.setAttribute("class", "restart button hidden");
        
        var mute = document.createElement("button");
        mute.setAttribute("class", "mute button");
        
        var unmute = document.createElement("button");
        unmute.setAttribute("class", "unmute button hidden");

        var prog = document.createElement("div");
        prog.setAttribute("class", "progress");
        
        var progPoint = document.createElement("span");
        progPoint.setAttribute("class", "progresspoint");
        
        
        prog.addEventListener('click', function(e) {
            if (e.target.classList.contains("progress")) {
                var left = e.clientX - getOffsetLeft(e.target);
                var perc = left / e.target.offsetWidth;
                var video = e.target.parentElement.parentElement.querySelector("video.video.object");
                var bar = e.target.querySelector("span.progressbar");
                var newTime = video.duration * perc;
                video.currentTime = newTime;
                
                bar.style.width = (perc * 100) + "%";
                
                var parent = video.parentElement;

                canvas = parent.getElementsByClassName('video-canvas')[0];
                ctx = canvas.getContext('2d');

                ctx.drawImage(video, 0, 0, 600, 460);
            }
        });
        
        
        progPoint.addEventListener('mousedown', function() {
            var player = this.parentElement.parentElement.parentElement.parentElement;
            var video = player.querySelector("video.video.object");
            this.setAttribute("data-playing", video.playing);
            video.pause();
            
            this.setAttribute("data-mousedown", "true");
        }, false);
        
        document.addEventListener('mousemove', function(e) {
            var _this = null;
            var all = document.body.querySelectorAll(".player.container .controls.wrapper .progress .progressbar .progresspoint");
            for (x=0; x < all.length; x++) {
                if (all[x].getAttribute("data-mousedown") == "true") {
                    _this = all[x];
                    break;
                }
            }
            
            if (_this == null)
                return;
            
            
            var left = e.clientX;
            var lleft = getOffsetLeft(_this.parentElement);
            
            var total = left - lleft;
            
            
            if (total < _this.parentElement.parentElement.offsetWidth)
                _this.parentElement.style.width = total + "px";
            else
                _this.parentElement.style.width = _this.parentElement.parentElement.offsetWidth + "px";
            
            
            var player = _this.parentElement.parentElement.parentElement.parentElement;
            var video = player.querySelector("video.video.object");

            left = _this.offsetLeft + 15;
            total = _this.parentElement.parentElement.offsetWidth;

            var perc = left / total;
            var time = video.duration * perc;

            video.currentTime = time;
            
            var play = player.querySelector("button.play.button");
            var restart = player.querySelector("button.restart.button");
            var pause = player.querySelector("button.pause.button");
            
            var mouse = _this.getAttribute("data-playing");
            
            if (!video.ended) {
                restart.classList.add("hidden");
                
                if (mouse == "true") {
                    pause.classList.remove("hidden");
                    play.classList.add("hidden");
                } else {
                    pause.classList.add("hidden");
                    play.classList.remove("hidden");
                }
            }
            
            var parent = video.parentElement;

            canvas = parent.getElementsByClassName('video-canvas')[0];
            ctx = canvas.getContext('2d');

            ctx.drawImage(video, 0, 0, 600, 460);
        }, false);
        
        document.addEventListener('mouseup', function() {
            var all = document.querySelectorAll(".player.container .controls.wrapper .progress .progressbar .progresspoint");
            for (x=0; x < all.length; x++) {
                var _this = all[x];
                var player = _this.parentElement.parentElement.parentElement.parentElement;
                var video = player.querySelector("video.video.object");
                
                var left = _this.offsetLeft + 15;
                var total = _this.parentElement.parentElement.offsetWidth;
                
                var perc = left / total;
                var time = video.duration * perc;
                
                video.currentTime = time;
                
                if (_this.getAttribute("data-playing") == "true")
                    video.play();

                _this.setAttribute("data-mousedown", "false");
            }
        }, false);
        
        var progress = document.createElement("span");
        progress.setAttribute("class", "progressbar");
        progress.appendChild(progPoint);
        prog.appendChild(progress);
        
        var controls = document.createElement("div");
        controls.setAttribute("class", "controls wrapper");
        controls.appendChild(play);
        controls.appendChild(pause);
        controls.appendChild(restart);
        controls.appendChild(prog);
        controls.appendChild(mute);
        controls.appendChild(unmute);

        var video = document.createElement("div");
        video.setAttribute("class", "video wrapper");
        video.appendChild(obj);
        
        var canvas = document.createElement("canvas");
        canvas.width = 400;
        canvas.height = 217;
        canvas.classList.add("video-canvas");
        video.appendChild(canvas);

        var player = document.createElement("div");
        player.setAttribute("class", "player container");
        player.appendChild(video);
        player.appendChild(controls);

        return {
            play: play,
            pause: pause,
            restart: restart,
            mute: mute,
            unmute: unmute,
            progress: progress,
            controls: controls,
            video: video,
            player: player
        };
    }
    
    function getOffsetLeft( elem )
    {
        var offsetLeft = 0;
        do {
          if ( !isNaN( elem.offsetLeft ) )
          {
              offsetLeft += elem.offsetLeft;
          }
        } while( elem = elem.offsetParent );
        return offsetLeft;
    }
    
    
    
    window.player = player;
})();