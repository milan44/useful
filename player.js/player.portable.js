Object.defineProperty(HTMLMediaElement.prototype, 'playing', {
    get: function(){
        return !!(this.currentTime > 0 && !this.paused && !this.ended && this.readyState > 2);
    }
});

(function() {
    var style = '.player{display:inline-block;position:relative}.player .controls.wrapper{display:block;box-sizing:border-box;position:absolute;height:40px;width:100%;bottom:0;left:0;z-index:20;background:rgba(166,166,166,.8)}.player .video{cursor:pointer}.player .video.wrapper{position:relative;z-index:10}.player canvas.video-canvas{position:absolute;transform:scale(1.4);-webkit-filter:blur(60px);filter:blur(60px);opacity:1;z-index:-1;top:0;left:0;height:100%!important;width:100%!important}.player .button{border:none;width:30px;height:30px;display:inline-block;background-color:transparent;cursor:pointer;margin:5px;background-size:30px;background-position:center;-webkit-box-shadow:0 0 5px 0 rgba(0,0,0,.75);-moz-box-shadow:0 0 5px 0 rgba(0,0,0,.75);box-shadow:0 0 5px 0 rgba(0,0,0,.75);border-radius:50%}.player .hidden{display:none}.player .progress{position:relative;box-sizing:border-box;height:5px;margin-left:15px;width:calc(100% - 100px);border-radius:5px;background:#fff;display:inline-block;cursor:pointer}.player .progress .progressbar{position:absolute;display:inline-block;top:0;left:0}.player .progress .progressbar .progresspoint{position:absolute;display:inline-block;height:15px;width:15px;right:0;background:#36f;border-radius:50%;transform:translateY(-25%) translateX(25%);cursor:pointer;-webkit-box-shadow:0 0 5px 0 rgba(0,0,0,.75);-moz-box-shadow:0 0 5px 0 rgba(0,0,0,.75);box-shadow:0 0 5px 0 rgba(0,0,0,.75)}.player .controls{opacity:0;transition:1s}.player:hover .controls{opacity:1;transition:.3s}.player .button.mute{box-shadow:none;background-image:url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/PjxzdmcgaGVpZ2h0PSIxMDI0IiB3aWR0aD0iODk2IiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPjxwYXRoIGQ9Ik0xMjggMzg0SDB2MjU2aDEyOGwyNTYgMTkyaDY0VjE5MmgtNjRMMTI4IDM4NHpNNTM4LjUxIDQyMS40OWMtMTIuNDk2LTEyLjQ5Ny0zMi43NTgtMTIuNDk3LTQ1LjI1NSAwLTEyLjQ5NiAxMi40OTYtMTIuNDk2IDMyLjc1OCAwIDQ1LjI1NSAyNC45OTQgMjQuOTkzIDI0Ljk5NCA2NS41MTYgMCA5MC41MS0xMi40OTYgMTIuNDk2LTEyLjQ5NiAzMi43NTggMCA0NS4yNTUgMTIuNDk3IDEyLjQ5NiAzMi43NTkgMTIuNDk2IDQ1LjI1NSAwQzU4OC40OTcgNTUyLjUyMSA1ODguNDk3IDQ3MS40NzcgNTM4LjUxIDQyMS40OXpNNjI5LjAyIDMzMC45ODFjLTEyLjQ5NS0xMi40OTctMzIuNzU4LTEyLjQ5Ny00NS4yNTUgMC0xMi40OTUgMTIuNDk2LTEyLjQ5NSAzMi43NTggMCA0NS4yNTUgNzQuOTgxIDc0Ljk4IDc0Ljk4MSAxOTYuNTQ4IDAgMjcxLjUyOC0xMi40OTUgMTIuNDk3LTEyLjQ5NSAzMi43NiAwIDQ1LjI1NiAxMi40OTcgMTIuNDk2IDMyLjc2IDEyLjQ5NiA0NS4yNTUgMEM3MjguOTk0IDU5My4wNDYgNzI4Ljk5NCA0MzAuOTU1IDYyOS4wMiAzMzAuOTgxek03MTkuNTI5IDI0MC40NzFjLTEyLjQ5Ny0xMi40OTctMzIuNzYtMTIuNDk3LTQ1LjI1NSAwLTEyLjQ5NiAxMi40OTYtMTIuNDk2IDMyLjc1OCAwIDQ1LjI1NSAxMjQuOTY4IDEyNC45NjggMTI0Ljk2OCAzMjcuNTggMCA0NTIuNTQ4LTEyLjQ5NiAxMi40OTctMTIuNDk2IDMyLjc1OSAwIDQ1LjI1NSAxMi40OTUgMTIuNDk3IDMyLjc1OCAxMi40OTcgNDUuMjU1IDBDODY5LjQ5IDYzMy41NjcgODY5LjQ5IDM5MC40MzIgNzE5LjUyOSAyNDAuNDcxeiIvPjwvc3ZnPg==);margin-left:10px}.player .button.unmute{box-shadow:none;background-image:url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/PjxzdmcgaGVpZ2h0PSIxMDI0IiB3aWR0aD0iODk2IiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPjxwYXRoIGQ9Ik0xMjggMzg0SDB2MjU2aDEyOGwyNTYgMTkyaDY0VjE5MmgtNjRMMTI4IDM4NHpNODY0IDQxNmwtNjQtNjQtOTYgOTYtOTYtOTYtNjMgNjMuNSA5NSA5Ni41LTk2IDk2IDY0IDY0IDk2LTk2IDk2IDk2IDY0LTY0LTk2LTk2TDg2NCA0MTZ6Ii8+PC9zdmc+);margin-left:10px}.player .button.play{background-image:url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/PjxzdmcgaGVpZ2h0PSIyMHB4IiB2ZXJzaW9uPSIxLjEiIHZpZXdCb3g9IjAgMCAyMCAyMCIgd2lkdGg9IjIwcHgiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6c2tldGNoPSJodHRwOi8vd3d3LmJvaGVtaWFuY29kaW5nLmNvbS9za2V0Y2gvbnMiIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIj48dGl0bGUvPjxkZXNjLz48ZGVmcy8+PGcgZmlsbD0ibm9uZSIgZmlsbC1ydWxlPSJldmVub2RkIiBpZD0iUGFnZS0xIiBzdHJva2U9Im5vbmUiIHN0cm9rZS13aWR0aD0iMSI+PGcgZmlsbD0iIzAwMDAwMCIgaWQ9Ikljb25zLUFWIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtMTI2LjAwMDAwMCwgLTg1LjAwMDAwMCkiPjxnIGlkPSJwbGF5LWNpcmNsZS1maWxsIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgxMjYuMDAwMDAwLCA4NS4wMDAwMDApIj48cGF0aCBkPSJNMTAsMCBDNC41LDAgMCw0LjUgMCwxMCBDMCwxNS41IDQuNSwyMCAxMCwyMCBDMTUuNSwyMCAyMCwxNS41IDIwLDEwIEMyMCw0LjUgMTUuNSwwIDEwLDAgTDEwLDAgWiBNOCwxNC41IEw4LDUuNSBMMTQsMTAgTDgsMTQuNSBMOCwxNC41IFoiIGlkPSJTaGFwZSIvPjwvZz48L2c+PC9nPjwvc3ZnPg==)}.player .button.restart{background-repeat:no-repeat;background-size:25px;box-shadow:none;background-image:url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/PjxzdmcgaGVpZ2h0PSIyMHB4IiB2ZXJzaW9uPSIxLjEiIHZpZXdCb3g9IjAgMCAxNiAyMCIgd2lkdGg9IjE2cHgiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6c2tldGNoPSJodHRwOi8vd3d3LmJvaGVtaWFuY29kaW5nLmNvbS9za2V0Y2gvbnMiIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIj48dGl0bGUvPjxkZXNjLz48ZGVmcy8+PGcgZmlsbD0ibm9uZSIgZmlsbC1ydWxlPSJldmVub2RkIiBpZD0iUGFnZS0xIiBzdHJva2U9Im5vbmUiIHN0cm9rZS13aWR0aD0iMSI+PGcgZmlsbD0iIzAwMDAwMCIgaWQ9Ikljb25zLUFWIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtMi4wMDAwMDAsIC0xMjcuMDAwMDAwKSI+PGcgaWQ9InJlcGxheSIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMi4wMDAwMDAsIDEyNy4wMDAwMDApIj48cGF0aCBkPSJNOCw0IEw4LDAgTDMsNSBMOCwxMCBMOCw2IEMxMS4zLDYgMTQsOC43IDE0LDEyIEMxNCwxNS4zIDExLjMsMTggOCwxOCBDNC43LDE4IDIsMTUuMyAyLDEyIEwwLDEyIEMwLDE2LjQgMy42LDIwIDgsMjAgQzEyLjQsMjAgMTYsMTYuNCAxNiwxMiBDMTYsNy42IDEyLjQsNCA4LDQgTDgsNCBaIiBpZD0iU2hhcGUiLz48L2c+PC9nPjwvZz48L3N2Zz4=)}.player .button.pause{background-image:url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/PjxzdmcgaGVpZ2h0PSIxNnB4IiB2ZXJzaW9uPSIxLjEiIHZpZXdCb3g9IjAgMCAxNiAxNiIgd2lkdGg9IjE2cHgiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6c2tldGNoPSJodHRwOi8vd3d3LmJvaGVtaWFuY29kaW5nLmNvbS9za2V0Y2gvbnMiIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIj48dGl0bGUvPjxkZWZzLz48ZyBmaWxsPSJub25lIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiIGlkPSJJY29ucyB3aXRoIG51bWJlcnMiIHN0cm9rZT0ibm9uZSIgc3Ryb2tlLXdpZHRoPSIxIj48ZyBmaWxsPSIjMDAwMDAwIiBpZD0iR3JvdXAiIHRyYW5zZm9ybT0idHJhbnNsYXRlKC0yODguMDAwMDAwLCAtNTI4LjAwMDAwMCkiPjxwYXRoIGQ9Ik0yOTYsNTQ0IEMyOTEuNTgxNzIyLDU0NCAyODgsNTQwLjQxODI3OCAyODgsNTM2IEMyODgsNTMxLjU4MTcyMiAyOTEuNTgxNzIyLDUyOCAyOTYsNTI4IEMzMDAuNDE4Mjc4LDUyOCAzMDQsNTMxLjU4MTcyMiAzMDQsNTM2IEMzMDQsNTQwLjQxODI3OCAzMDAuNDE4Mjc4LDU0NCAyOTYsNTQ0IEwyOTYsNTQ0IFogTTI5OSw1MzIgTDI5Nyw1MzIgTDI5Nyw1MzYgTDI5Nyw1NDAgTDI5OSw1NDAgTDI5OSw1MzIgTDI5OSw1MzIgWiBNMjk1LDUzMiBMMjkzLDUzMiBMMjkzLDUzNiBMMjkzLDU0MCBMMjk1LDU0MCBMMjk1LDUzMiBMMjk1LDUzMiBaIE0yOTUsNTMyIiBpZD0iU2hhcGUgY29weSAzIi8+PC9nPjwvZz48L3N2Zz4=)}';
    var el = document.createElement("style");
    el.innerHTML = style;
    document.head.appendChild(el);
})();

(function() {
    var player = function(video, player, ambilight) {
        this.vidobj = null;
        this.html = null;
        this.light = ambilight;
        this.player = player;
        this.vidurl = video;
        this.timeout = null;
        this.create = function() {
            this.player = document.querySelector(this.player);
            
            var obj = document.createElement("video");
            obj.src = this.vidurl;
            obj.controls = false;
            
            obj.addEventListener('ended',this.ended,false);
            
            obj.setAttribute("class", "video object");
            
            obj.addEventListener("click", this.video, false);
            
            var vid = this.video;
            
            obj.onkeyup = function(e){
                if(e.keyCode == 32){
                    vid(null, obj);
                }
            }
            
            var html = getHTML(obj);
            this.player.appendChild(html.player);
            
            if (this.light)
                this.ambilight(obj);
            
            html.play.onclick = this.play;
            html.pause.onclick = this.pause;
            html.restart.onclick = this.restart;
            
            html.mute.onclick = this.mute;
            html.unmute.onclick = this.unmute;
            
            this.vidobj = obj;
            this.html = html;
            
            var parent = obj.parentElement;

            canvas = parent.getElementsByClassName('video-canvas')[0];
            ctx = canvas.getContext('2d');

            ctx.drawImage(obj, 0, 0, 600, 460);
            
            this.update(this);
        };
        
        this.ambilight = function (videoobj) {
            var timerID;
            var parent = videoobj.parentElement;

            canvas = parent.getElementsByClassName('video-canvas')[0];
            ctx = canvas.getContext('2d');

            var video = videoobj;

            video.addEventListener('play', function() {
                timerID = window.setInterval(function() {
                    ctx.drawImage(video, 0, 0, 600, 460);
                }, 100);
            });

            video.addEventListener('pause', function() {
                clearInterval(timerID);
                ctx.drawImage(video, 0, 0, 600, 460);
            });

            video.addEventListener('ended', function() {
                clearInterval(timerID);
                ctx.drawImage(video, 0, 0, 600, 460);
            });
        };
        
        this.video = function(asdf, _this) {
            if (_this == null)
                _this = this;
            if (_this.playing) {
                var player = _this.parentElement.parentElement;
                var pause = player.querySelector("button.pause.button");
                pause.click();
            } else {
                var player = _this.parentElement.parentElement;
                var play = player.querySelector("button.play.button");
                play.click();
            }
        };
        
        this.mute = function() {
            var player = this.parentElement.parentElement;
            var mute = this.parentElement.querySelector("button.mute.button");
            var unmute = this.parentElement.querySelector("button.unmute.button");
            var vidobj = player.querySelector("video.video.object");
            
            vidobj.muted = true;
            
            mute.classList.add("hidden");
            unmute.classList.remove("hidden");
        };
        this.unmute = function() {
            var player = this.parentElement.parentElement;
            var mute = this.parentElement.querySelector("button.mute.button");
            var unmute = this.parentElement.querySelector("button.unmute.button");
            var vidobj = player.querySelector("video.video.object");
            
            vidobj.muted = false;
            
            mute.classList.remove("hidden");
            unmute.classList.add("hidden");
        };
        
        this.restart = function() {
            var player = this.parentElement.parentElement;
            var pause = this.parentElement.querySelector("button.pause.button");
            var play = this.parentElement.querySelector("button.play.button");
            var vidobj = player.querySelector("video.video.object");
            
            vidobj.currentTime = 0;
            vidobj.play();
            
            this.classList.add("hidden");
            pause.classList.remove("hidden");
            play.classList.add("hidden");
        };
        this.play = function() {
            var player = this.parentElement.parentElement;
            var pause = this.parentElement.querySelector("button.pause.button");
            var vidobj = player.querySelector("video.video.object");
            
            vidobj.pause();
            
            vidobj.play();
            this.classList.add("hidden");
            pause.classList.remove("hidden");
        };
        this.pause = function() {
            var player = this.parentElement.parentElement;
            var play = this.parentElement.querySelector("button.play.button");
            var vidobj = player.querySelector("video.video.object");
            vidobj.pause();
            this.classList.add("hidden");
            play.classList.remove("hidden");
        };
        this.update = function(_this) {
            if (_this.vidobj.playing) {
                _this.updateProgressBar();
            }
            
            _this.timeout = setTimeout(_this.update, 100, _this);
        };
        this.getProgress = function() {
            var totalLength = this.vidobj.duration;
            var playedLength = this.vidobj.currentTime;
            
            var percentage = (playedLength / totalLength) * 100;
            return percentage;
        };
        this.updateProgressBar = function() {
            this.html.progress.style.width = this.getProgress() + "%";
        };
        this.ended = function() {
            var player = this.parentElement.parentElement;
            var play = player.querySelector("button.play.button");
            var restart = player.querySelector("button.restart.button");
            var pause = player.querySelector("button.pause.button");
            var vidobj = player.querySelector("video.video.object");
            pause.classList.add("hidden");
            play.classList.add("hidden");
            restart.classList.remove("hidden");
        };
    };
    
    function getHTML(obj) {
        var play = document.createElement("button");
        play.setAttribute("class", "play button");

        var pause = document.createElement("button");
        pause.setAttribute("class", "pause button hidden");
        
        var restart = document.createElement("button");
        restart.setAttribute("class", "restart button hidden");
        
        var mute = document.createElement("button");
        mute.setAttribute("class", "mute button");
        
        var unmute = document.createElement("button");
        unmute.setAttribute("class", "unmute button hidden");

        var prog = document.createElement("div");
        prog.setAttribute("class", "progress");
        
        var progPoint = document.createElement("span");
        progPoint.setAttribute("class", "progresspoint");
        
        
        prog.addEventListener('click', function(e) {
            if (e.target.classList.contains("progress")) {
                var left = e.clientX - getOffsetLeft(e.target);
                var perc = left / e.target.offsetWidth;
                var video = e.target.parentElement.parentElement.querySelector("video.video.object");
                var bar = e.target.querySelector("span.progressbar");
                var newTime = video.duration * perc;
                video.currentTime = newTime;
                
                bar.style.width = (perc * 100) + "%";
                
                var parent = video.parentElement;

                canvas = parent.getElementsByClassName('video-canvas')[0];
                ctx = canvas.getContext('2d');

                ctx.drawImage(video, 0, 0, 600, 460);
            }
        });
        
        
        progPoint.addEventListener('mousedown', function() {
            var player = this.parentElement.parentElement.parentElement.parentElement;
            var video = player.querySelector("video.video.object");
            this.setAttribute("data-playing", video.playing);
            video.pause();
            
            this.setAttribute("data-mousedown", "true");
        }, false);
        
        document.addEventListener('mousemove', function(e) {
            var _this = null;
            var all = document.body.querySelectorAll(".player.container .controls.wrapper .progress .progressbar .progresspoint");
            for (x=0; x < all.length; x++) {
                if (all[x].getAttribute("data-mousedown") == "true") {
                    _this = all[x];
                    break;
                }
            }
            
            if (_this == null)
                return;
            
            
            var left = e.clientX;
            var lleft = getOffsetLeft(_this.parentElement);
            
            var total = left - lleft;
            
            
            if (total < _this.parentElement.parentElement.offsetWidth)
                _this.parentElement.style.width = total + "px";
            else
                _this.parentElement.style.width = _this.parentElement.parentElement.offsetWidth + "px";
            
            
            var player = _this.parentElement.parentElement.parentElement.parentElement;
            var video = player.querySelector("video.video.object");

            left = _this.offsetLeft + 15;
            total = _this.parentElement.parentElement.offsetWidth;

            var perc = left / total;
            var time = video.duration * perc;

            video.currentTime = time;
            
            var play = player.querySelector("button.play.button");
            var restart = player.querySelector("button.restart.button");
            var pause = player.querySelector("button.pause.button");
            
            var mouse = _this.getAttribute("data-playing");
            
            if (!video.ended) {
                restart.classList.add("hidden");
                
                if (mouse == "true") {
                    pause.classList.remove("hidden");
                    play.classList.add("hidden");
                } else {
                    pause.classList.add("hidden");
                    play.classList.remove("hidden");
                }
            }
            
            var parent = video.parentElement;

            canvas = parent.getElementsByClassName('video-canvas')[0];
            ctx = canvas.getContext('2d');

            ctx.drawImage(video, 0, 0, 600, 460);
        }, false);
        
        document.addEventListener('mouseup', function() {
            var all = document.querySelectorAll(".player.container .controls.wrapper .progress .progressbar .progresspoint");
            for (x=0; x < all.length; x++) {
                var _this = all[x];
                var player = _this.parentElement.parentElement.parentElement.parentElement;
                var video = player.querySelector("video.video.object");
                
                var left = _this.offsetLeft + 15;
                var total = _this.parentElement.parentElement.offsetWidth;
                
                var perc = left / total;
                var time = video.duration * perc;
                
                video.currentTime = time;
                
                if (_this.getAttribute("data-playing") == "true")
                    video.play();

                _this.setAttribute("data-mousedown", "false");
            }
        }, false);
        
        var progress = document.createElement("span");
        progress.setAttribute("class", "progressbar");
        progress.appendChild(progPoint);
        prog.appendChild(progress);
        
        var controls = document.createElement("div");
        controls.setAttribute("class", "controls wrapper");
        controls.appendChild(play);
        controls.appendChild(pause);
        controls.appendChild(restart);
        controls.appendChild(prog);
        controls.appendChild(mute);
        controls.appendChild(unmute);

        var video = document.createElement("div");
        video.setAttribute("class", "video wrapper");
        video.appendChild(obj);
        
        var canvas = document.createElement("canvas");
        canvas.width = 400;
        canvas.height = 217;
        canvas.classList.add("video-canvas");
        video.appendChild(canvas);

        var player = document.createElement("div");
        player.setAttribute("class", "player container");
        player.appendChild(video);
        player.appendChild(controls);

        return {
            play: play,
            pause: pause,
            restart: restart,
            mute: mute,
            unmute: unmute,
            progress: progress,
            controls: controls,
            video: video,
            player: player
        };
    }
    
    function getOffsetLeft( elem )
    {
        var offsetLeft = 0;
        do {
          if ( !isNaN( elem.offsetLeft ) )
          {
              offsetLeft += elem.offsetLeft;
          }
        } while( elem = elem.offsetParent );
        return offsetLeft;
    }
    
    
    
    window.player = player;
})();