/**
 * Draggable made by milan44
 * 
 * https://gitlab.com/milan44
 */
(function() {
	var draggable = function (config) {
		
		var elem = config.element,
			collide = config.collide,
			glide = config.glide,
			border = config.border;
		
		var down = false;
		var x = 0;
		var y = 0;
		var active = true;
		
		elem.classList.add("draggable_collider");
	
		function getPos(el) {
		    // yay readability
		    for (var lx=0, ly=0;
		         el != null;
		         lx += el.offsetLeft, ly += el.offsetTop, el = el.offsetParent);
		    return {x: lx,y: ly};
		}
	
		function element() {
			return elem;
		}
		
		elem.ondblclick = function() {
			if (active)
				active = false;
			else
				active = true;
		};
	
		elem.onmousedown = function(e) {
			down = true;
			pos = getPos(element());
	
			x = pos.x-e.clientX;
			y = pos.y-e.clientY;
		};
		document.addEventListener("mouseup", function() {
			down = false;
		});
	
		document.addEventListener("mousemove", function(e) {
			if (down && active) {
				el = element().cloneNode(true);
				
				el.style.opacity = "0";
				el.classList.add(Date.now());
				
				document.body.appendChild(el);
				
				var sideside = (e.clientX+x)+"px";
				var toptop = (e.clientY+y)+"px";
				
				el.style.left = sideside;
				el.style.top = toptop;
				
				if ((checkOverlap(el, element()) || !collide) && (checkBorder(el) || !border)) {
					element().style.left = sideside;
					element().style.top = toptop;
				} else if (glide) {
					pos = getPos(element());
					x = pos.x-e.clientX;
					y = pos.y-e.clientY;
				}
				
				document.body.removeChild(el);
			}
		});
	};
	
	var draggableX = function (config) {
		
		var elem = config.element,
			collide = config.collide,
			glide = config.glide,
			border = config.border;
		
		var down = false;
		var x = 0;
		var active = true;
		
		elem.classList.add("draggable_collider");
	
		function getPos(el) {
		    // yay readability
		    for (var lx=0, ly=0;
		         el != null;
		         lx += el.offsetLeft, ly += el.offsetTop, el = el.offsetParent);
		    return {x: lx,y: ly};
		}
		
		elem.ondblclick = function() {
			if (active)
				active = false;
			else
				active = true;
		};
	
		function element() {
			return elem;
		}
	
		elem.onmousedown = function(e) {
			down = true;
			pos = getPos(element());
	
			x = pos.x-e.clientX;
		};
		document.addEventListener("mouseup", function() {
			down = false;
		});
	
		document.addEventListener("mousemove", function(e) {
			if (down && active) {
				el = element().cloneNode(true);
				
				el.style.opacity = "0";
				el.classList.add(Date.now());
				
				document.body.appendChild(el);
				
				var sideside = (e.clientX+x)+"px";
				
				el.style.left = sideside;
				
				if ((checkOverlap(el, element()) || !collide) && (checkBorder(el) || !border)) {
					element().style.left = sideside;
				} else if (glide) {
					pos = getPos(element());
					x = pos.x-e.clientX;
				}
				
				document.body.removeChild(el);
			}
		});
	};
	
	var draggableY = function (config) {
		
		var elem = config.element,
			collide = config.collide,
			glide = config.glide,
			border = config.border;
		
		var down = false;
		var y = 0;
		var active = true;
		
		elem.classList.add("draggable_collider");
	
		function getPos(el) {
		    // yay readability
		    for (var lx=0, ly=0;
		         el != null;
		         lx += el.offsetLeft, ly += el.offsetTop, el = el.offsetParent);
		    return {x: lx,y: ly};
		}
		
		elem.ondblclick = function() {
			if (active)
				active = false;
			else
				active = true;
		};
	
		function element() {
			return elem;
		}
	
		elem.onmousedown = function(e) {
			down = true;
			pos = getPos(element());
	
			y = pos.y-e.clientY;
		};
		document.addEventListener("mouseup", function() {
			down = false;
		});
	
		document.addEventListener("mousemove", function(e) {
			if (down && active) {
				el = element().cloneNode(true);
				
				el.style.opacity = "0";
				el.classList.add(Date.now());
				
				document.body.appendChild(el);
				
				var toptop = (e.clientY+y)+"px";
				
				el.style.top = toptop;
				
				if (!border) {
					
				}
				
				if ((checkOverlap(el, element()) || !collide) && (checkBorder(el) || !border)) {
					element().style.top = toptop;
				} else if (glide) {
					pos = getPos(element());
					y = pos.y-e.clientY;
				}
				
				document.body.removeChild(el);
			}
		});
	};
	
	function checkBorder(el) {
		var main = el.getBoundingClientRect();
		
		var other = {
			top: 0,
			left: 0,
			right: window.innerWidth,
			bottom: window.innerHeight
		};
		
		return ((main.bottom < other.bottom) && (main.top > other.top) && (main.left > other.left) && (main.right < other.right));
	}
	checkBorder(document.getElementById("all"))
	
	function checkOverlap(el, not) {
		var main = el.getBoundingClientRect();
		
		var other = document.getElementsByClassName("draggable_collider");
		
		for (x=0;x<other.length;x++) {
			rect = other[x].getBoundingClientRect();
			
			overlap = ((main.right > rect.left) && (main.left < rect.right)) && ((main.top < rect.bottom) && (main.bottom > rect.top));
			
			if (overlap && other[x] != el && other[x] != not)
				return false;
		}
		return true;
	}
	
	window.draggable = draggable;
	window.draggableX = draggableX;
	window.draggableY = draggableY;
})();