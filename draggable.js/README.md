# Explanation

Draggable makes any Html element draggable.

On doubleclick they are not moveable anymore until they are doubleclicked again.

## Usage

If you want only to move in the X Axis:
```javascript
draggableX(config);
```

If you want only to move in the Y Axis:
```javascript
draggableY(config);
```

If you want to move in all directions:
```javascript
draggable(config);
```

### Config

There are 4 properties of the config (these are the default values):

```javascript
var config = {
    element: document.getElementById("myelement"),
	collide: false,
	glide: false,
	border: false
};
```

`element` is the element which you want to be draggable.  
`collide` specifies if the elements should collide.  
`glide` specifies if the element should jump to the cursor or not.  
`border` specifies if the element can be dragged outside of the window.  

### [Demo](https://milan44.gitlab.io/useful/draggable.html)