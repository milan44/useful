# Explanation

error.php is a PHP Script for HTTP Error Handling.

### GET Aparameters

error.php accepts only one Parameter. With this Parameter you can specify the HTTP Status Code:

`error.php?x=404`  
Will show information to the HTTP Status Code: 404

### Used like:

.htaccess:
```
ErrorDocument 404 error.php
```

### [Example Htaccess](example.htaccess)

### [Demo](https://milan44.gitlab.io/useful/error.html)