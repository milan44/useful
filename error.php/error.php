<?php
$filename = $_SERVER['SCRIPT_NAME'];

echo '<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">';
echo '<link rel="preconnect" href="https://fonts.wiese2.org" />
<link rel="stylesheet" href="https://fonts.wiese2.org/cdn/cache/Ubuntu_Mono/400" />';
echo '<style>
body {
    font-family: \'Ubuntu Mono\', monospace, sans-serif;
    margin: 0;
    background: black;
    color: white;
}
a {
    color: #39ACDB !important;
}
.paragraph:not(.history) {
    box-sizing: border-box;
    display: inline-block;
    width: calc(50% - 20px);
}
.paragraph-wrapper {
    display: flex;
    justify-content: space-between;
}
h2 {
    margin-top: 20px;
    font-size: 30px;
}
h3 {
    padding-left: 20px;
}
#page {
    max-width: 1400px;
    margin: auto;
    padding: 10px;
    text-align: justify;
}
img {
    max-width: 100%;
    width: 350px;
    display: block;
}
.image-wrapper {
    display: flex;
    margin-top: 30px;
}
</style>';

$id = $_GET["x"];

if (!isset($_GET["x"])) $id = $_SERVER["REDIRECT_STATUS"];

if ($id == "") {
    $id = "200";
}

if (!is_numeric($id)) {
    die("Invalid status code!");
}

echo '<title>'.$id.' - '.geter("$id").'</title>';

echo '<div id="page">';

echo "<h2>Http Code ".$id.", ".geter($id)."</h2>";

echo '<div class="image-wrapper">';
echo '<img src="https://http.cat/'.$id.'" />';
echo '<div class="paragraph history"><h3>History</h3>
In HTTP/1.0 and since, the first line of the HTTP response is called the status line and includes a numeric status code (such as "<a href="'.$filename.'?x=404">404</a>") and a textual reason phrase (such as "Not Found"). The way the user agent handles the response primarily depends on the code and secondarily on the other response header fields. Custom status codes can be used since, if the user agent encounters a code it does not recognize, it can use the first digit of the code to determine the general class of the response.<br><br><a href="https://en.wikipedia.org/wiki/List_of_HTTP_status_codes" target="_blank">More Info.</a></div>';
echo '</div>';

echo '<div class="paragraph-wrapper">';
echo '<div class="paragraph information"><h3>Information</h3>';
echo getinfo($id) . '</div>';
echo '<div class="paragraph category">' . is("$id") . '</div>';
echo '</div>';

echo '</div>';

function is($id) {
    if (geter($id) == "Unknown Status Code!")
        return "<h3>Unknown</h3>";
    
    $s = substr($id." ", 0, 1);
    switch ($s) {
        case "1":
            return '<h3>1xx Informational responses</h3>
            An informational response indicates that the request was received and understood. It is issued on a provisional basis while request processing continues. It alerts the client to wait for a final response. The message consists only of the status line and optional header fields, and is terminated by an empty line. As the HTTP/1.0 standard did not define any 1xx status codes, servers must not send a 1xx response to an HTTP/1.0 compliant client except under experimental conditions.';
        case "2":
            return '<h3>2xx Success</h3>
            This class of status codes indicates the action requested by the client was received, understood and accepted.';
        case "3":
            return '<h3>3xx Redirection</h3>
            This class of status code indicates the client must take additional action to complete the request. Many of these status codes are used in URL redirection.<br>
            <br>
            A user agent may carry out the additional action with no user interaction only if the method used in the second request is GET or HEAD. A user agent may automatically redirect a request. A user agent should detect and intervene to prevent cyclical redirects.';
        case "4":
            return '<h3>4xx Client errors</h3>
            This class of status code is intended for situations in which the error seems to have been caused by the client. Except when responding to a HEAD request, the server should include an entity containing an explanation of the error situation, and whether it is a temporary or permanent condition. These status codes are applicable to any request method. User agents should display any included entity to the user.';
        case "5":
            return '<h3>5xx Server errors</h3>
            The server failed to fulfil a request.<br>
            <br>
            Response status codes beginning with the digit "5" indicate cases in which the server is aware that it has encountered an error or is otherwise incapable of performing the request. Except when responding to a HEAD request, the server should include an entity containing an explanation of the error situation, and indicate whether it is a temporary or permanent condition. Likewise, user agents should display any included entity to the user. These response codes are applicable to any request method.';
        default:
            return '<h3>Unknown</h3>';
    }
}

function geter($id) {
    switch ($id) {
                                    // 1xx Informational responses
        case "100":
            return "Continue";
        case "101":
            return "Switching Protocols";
        case "102":
            return "Processing";
        case "103":
            return "Early Hints";
                                    // 2xx Successfull Operations
        case "200":
            return "OK";
        case "201":
            return "Created";
        case "202":
            return "Accepted";
        case "203":
            return "Non-Authoritative Information";
        case "204":
            return "No Content";
        case "205":
            return "Reset Content";
        case "206":
            return "Partial Content";
        case "207":
            return "Multi-Status";
        case "208":
            return "Already Reported";
        case "226":
            return "IM Used";
                                    // 3xx Redictions
        case "300":
            return "Multiple Choices";
        case "301":
            return "Moved Permanently";
        case "302":
            return "Found (Moved Temporarily)";
        case "303":
            return "See Other";
        case "304":
            return "Not Modified";
        case "305":
            return "Use Proxy";
        case "306":
            return "(reserved)";
        case "307":
            return "Temporary Redict";
        case "308":
            return "Permanent Redict";
                                    // 4xx Client-Errors
        case "400":
            return "Bad Request";
        case "401":
            return "Unauthorized";
        case "402":
            return "Payment Required";
        case "403":
            return "Forbidden";
        case "404":
            return "Not Found";
        case "405":
            return "Method not allowed";
        case "406":
            return "Not Acceptable";
        case "407":
            return "Proxy Authentication Required";
        case "408":
            return "Request Timeout";
        case "409":
            return "Conflict";
        case "410":
            return "Gone";
        case "411":
            return "Length Required";
        case "412":
            return "Precondition Failed";
        case "413":
            return "Payload Too Large";
        case "414":
            return "URI Too Long";
        case "415":
            return "Unsupported Media Type";
        case "416":
            return "Range not satisfiable";
        case "417":
            return "Expectation Failed";
        case "421":
            return "Misdirected Request";
        case "422":
            return "Unprocessable Entity";
        case "423":
            return "Locked";
        case "424":
            return "Failed Dependency";
        case "426":
            return "Upgrade Required";
        case "428":
            return "Procontidion Required";
        case "429":
            return "Too Many Requests";
        case "431":
            return "Request Header Fields Too Large";
        case "451":
            return "Unavailable For Legal Reasons";
        case "418":
            return "I´m a teapot";
        case "425":
            return "Unordered Collection";
                                    // 5xx Server-Errors
        case "500":
            return "Internal Server Error";
        case "501":
            return "Not Implemented";
        case "502":
            return "Bad Gateway";
        case "503":
            return "Service Unavailable";
        case "504":
            return "Gateway Timeout";
        case "505":
            return "HTTP Version not supported";
        case "506":
            return "Variant Also Negotiates";
        case "507":
            return "Insufficient Storage";
        case "508":
            return "Loop detected";
        case "510":
            return "Not Extended";
        case "511":
            return "Network Authentication Required";
            
        default:
            return "Unknown Status Code!";
    }
}

function getinfo($id) {
    switch ($id) {
                                    // 1xx Informational responses
        case "100":
            return "The server has received the request headers and the client should proceed to send the request body (in the case of a request for which a body needs to be sent; for example, a POST request). Sending a large request body to a server after a request has been rejected for inappropriate headers would be inefficient. To have a server check the request's headers, a client must send Expect: <a href=".$filename."?x=100'>100</a>-continue as a header in its initial request and receive a <a href=".$filename."?x=100'>100</a> Continue status code in response before sending the body. If the client receives an error code such as <a href=".$filename."?x=403'>403</a> (Forbidden) or <a href=".$filename."?x=405'>405</a> (Method Not Allowed) then it shouldn't send the request's body. The response <a href=".$filename."?x=417'>417</a> Expectation Failed indicates that the request should be repeated without the Expect header as it indicates that the server doesn't support expectations (this is the case, for example, of HTTP/1.0 servers).";
        case "101":
            return "The requester has asked the server to switch protocols and the server has agreed to do so.";
        case "102":
            return "A WebDAV request may contain many sub-requests involving file operations, requiring a long time to complete the request. This code indicates that the server has received and is processing the request, but no response is available yet.This prevents the client from timing out and assuming the request was lost.";
        case "103":
            return "Used to return some response headers before file HTTP message.";
                                    // 2xx Successfull Operations
        case "200":
            return "Standard response for successful HTTP requests. The actual response will depend on the request method used. In a GET request, the response will contain an entity corresponding to the requested resource. In a POST request, the response will contain an entity describing or containing the result of the action.";
        case "201":
            return "The request has been fulfilled, resulting in the creation of a new resource.";
        case "202":
            return "The request has been accepted for processing, but the processing has not been completed. The request might or might not be eventually acted upon, and may be disallowed when processing occurs.";
        case "203":
            return "The server is a transforming proxy (e.g. a Web accelerator) that received a <a href=".$filename."?x=200'>200</a> OK from its origin, but is returning a modified version of the origin's response.";
        case "204":
            return "The server successfully processed the request and is not returning any content.";
        case "205":
            return "The server successfully processed the request, but is not returning any content. Unlike a <a href=".$filename."?x=204'>204</a> response, this response requires that the requester reset the document view.";
        case "206":
            return "The server is delivering only part of the resource (byte serving) due to a range header sent by the client. The range header is used by HTTP clients to enable resuming of interrupted downloads, or split a download into multiple simultaneous streams.";
        case "207":
            return "The message body that follows is by default an XML message and can contain a number of separate response codes, depending on how many sub-requests were made.";
        case "208":
            return "The members of a DAV binding have already been enumerated in a preceding part of the (multistatus) response, and are not being included again.";
        case "226":
            return "The server has fulfilled a request for the resource, and the response is a representation of the result of one or more instance-manipulations applied to the current instance.";
                                    // 3xx Redictions
        case "300":
            return "Indicates multiple options for the resource from which the client may choose (via agent-driven content negotiation). For example, this code could be used to present multiple video format options, to list files with different filename extensions, or to suggest word-sense disambiguation.";
        case "301":
            return "This and all future requests should be directed to the given URI.";
        case "302":
            return "This is an example of industry practice contradicting the standard. The HTTP/1.0 specification (RFC 1945) required the client to perform a temporary redirect (the original describing phrase was \"Moved Temporarily\"), but popular browsers implemented <a href=".$filename."?x=302'>302</a> with the functionality of a <a href=".$filename."?x=303'>303</a> See Other. Therefore, HTTP/1.1 added status codes <a href=".$filename."?x=303'>303</a> and <a href=".$filename."?x=307'>307</a> to distinguish between the two behaviours. However, some Web applications and frameworks use the 302 status code as if it were the <a href=".$filename."?x=303'>303</a>.";
        case "303":
            return "The response to the request can be found under another URI using the GET method. When received in response to a POST (or PUT/DELETE), the client should presume that the server has received the data and should issue a new GET request to the given URI.";
        case "304":
            return "Indicates that the resource has not been modified since the version specified by the request headers If-Modified-Since or If-None-Match. In such case, there is no need to retransmit the resource since the client still has a previously-downloaded copy.";
        case "305":
            return "The requested resource is available only through a proxy, the address for which is provided in the response. Many HTTP clients (such as Mozilla and Internet Explorer) do not correctly handle responses with this status code, primarily for security reasons.";
        case "306":
            return "No longer used. Originally meant \"Subsequent requests should use the specified proxy.\"";
        case "307":
            return "In this case, the request should be repeated with another URI; however, future requests should still use the original URI. In contrast to how <a href=".$filename."?x=302'>302</a> was historically implemented, the request method is not allowed to be changed when reissuing the original request. For example, a POST request should be repeated using another POST request.";
        case "308":
            return "The request and all future requests should be repeated using another URI. <a href=".$filename."?x=307'>307</a> and <a href=".$filename."?x=308'>308</a> parallel the behaviors of <a href=".$filename."?x=302'>302</a> and <a href=".$filename."?x=301'>301</a>, but do not allow the HTTP method to change. So, for example, submitting a form to a permanently redirected resource may continue smoothly.";
                                    // 4xx Client-Errors
        case "400":
            return "The server cannot or will not process the request due to an apparent client error (e.g., malformed request syntax, size too large, invalid request message framing, or deceptive request routing).";
        case "401":
            return "Similar to <a href=".$filename."?x=403'>403</a> Forbidden, but specifically for use when authentication is required and has failed or has not yet been provided. The response must include a WWW-Authenticate header field containing a challenge applicable to the requested resource. See Basic access authentication and Digest access authentication. <a href=".$filename."?x=401'>401</a> semantically means \"unauthenticated\", i.e. the user does not have the necessary credentials.<br>Note: Some sites issue HTTP <a href=".$filename."?x=401'>401</a> when an IP address is banned from the website (usually the website domain) and that specific address is refused permission to access a website.";
        case "402":
            return "Reserved for future use. The original intention was that this code might be used as part of some form of digital cash or micropayment scheme, as proposed for example by GNU Taler, but that has not yet happened, and this code is not usually used. Google Developers API uses this status if a particular developer has exceeded the daily limit on requests.";
        case "403":
            return "The request was valid, but the server is refusing action. The user might not have the necessary permissions for a resource, or may need an account of some sort.";
        case "404":
            return "The requested resource could not be found but may be available in the future. Subsequent requests by the client are permissible.";
        case "405":
            return "A request method is not supported for the requested resource; for example, a GET request on a form that requires data to be presented via POST, or a PUT request on a read-only resource.";
        case "406":
            return "The requested resource is capable of generating only content not acceptable according to the Accept headers sent in the request.";
        case "407":
            return "The client must first authenticate itself with the proxy.";
        case "408":
            return "The server timed out waiting for the request. According to HTTP specifications: \"The client did not produce a request within the time that the server was prepared to wait. The client MAY repeat the request without modifications at any later time.\"";
        case "409":
            return "Indicates that the request could not be processed because of conflict in the request, such as an edit conflict between multiple simultaneous updates.";
        case "410":
            return "Indicates that the resource requested is no longer available and will not be available again. This should be used when a resource has been intentionally removed and the resource should be purged. Upon receiving a <a href=".$filename."?x=410'>410</a> status code, the client should not request the resource in the future. Clients such as search engines should remove the resource from their indices. Most use cases do not require clients and search engines to purge the resource, and a \"<a href=".$filename."?x=404'>404</a> Not Found\" may be used instead.";
        case "411":
            return "The request did not specify the length of its content, which is required by the requested resource.";
        case "412":
            return "The server does not meet one of the preconditions that the requester put on the request.";
        case "413":
            return "The request is larger than the server is willing or able to process. Previously called \"Request Entity Too Large\".";
        case "414":
            return "The URI provided was too long for the server to process. Often the result of too much data being encoded as a query-string of a GET request, in which case it should be converted to a POST request. Called \"Request-URI Too Long\" previously.";
        case "415":
            return "The request entity has a media type which the server or resource does not support. For example, the client uploads an image as image/svg+xml, but the server requires that images use a different format.";
        case "416":
            return "The client has asked for a portion of the file (byte serving), but the server cannot supply that portion. For example, if the client asked for a part of the file that lies beyond the end of the file. Called \"Requested Range Not Satisfiable\" previously.";
        case "417":
            return "The server cannot meet the requirements of the Expect request-header field.";
        case "421":
            return "The request was directed at a server that is not able to produce a response (for example because of connection reuse).";
        case "422":
            return "The request was well-formed but was unable to be followed due to semantic errors.";
        case "423":
            return "The resource that is being accessed is locked.";
        case "424":
            return "The request failed because it depended on another request and that request failed (e.g., a PROPPATCH).";
        case "426":
            return "The client should switch to a different protocol such as TLS/1.0, given in the Upgrade header field.";
        case "428":
            return "The origin server requires the request to be conditional. Intended to prevent the 'lost update' problem, where a client GETs a resource's state, modifies it, and PUTs it back to the server, when meanwhile a third party has modified the state on the server, leading to a conflict.";
        case "429":
            return "The user has sent too many requests in a given amount of time. Intended for use with rate-limiting schemes.";
        case "431":
            return "The server is unwilling to process the request because either an individual header field, or all the header fields collectively, are too large.";
        case "451":
            return "A server operator has received a legal demand to deny access to a resource or to a set of resources that includes the requested resource. The code <a href=".$filename."?x=451'>451</a> was chosen as a reference to the novel Fahrenheit 451 (see the Acknowledgements in the RFC).";
        case "418":
            return "This code was defined in 1998 as one of the traditional IETF April Fools' jokes, in RFC 2324, Hyper Text Coffee Pot Control Protocol, and is not expected to be implemented by actual HTTP servers. The RFC specifies this code should be returned by teapots requested to brew coffee. This HTTP status is used as an Easter egg in some websites, including Google.com.";
        case "425":
            return "The <a href=".$filename."?x=425'>425</a> (Unordered Collection) status code indicates that the client attempted to set the position of an internal collection member in an unordered collection or in a collection with a server-maintained ordering.";
                                    // 5xx Server-Errors
        case "500":
            return "A generic error message, given when an unexpected condition was encountered and no more specific message is suitable.";
        case "501":
            return "The server either does not recognize the request method, or it lacks the ability to fulfil the request. Usually this implies future availability (e.g., a new feature of a web-service API).";
        case "502":
            return "The server was acting as a gateway or proxy and received an invalid response from the upstream server.";
        case "503":
            return "The server is currently unavailable (because it is overloaded or down for maintenance). Generally, this is a temporary state.";
        case "504":
            return "The server was acting as a gateway or proxy and did not receive a timely response from the upstream server.";
        case "505":
            return "The server does not support the HTTP protocol version used in the request.";
        case "506":
            return "Transparent content negotiation for the request results in a circular reference.";
        case "507":
            return "The server is unable to store the representation needed to complete the request.";
        case "508":
            return "The server detected an infinite loop while processing the request (sent in lieu of <a href=".$filename."?x=208'>208</a> Already Reported).";
        case "510":
            return "Further extensions to the request are required for the server to fulfil it.";
        case "511":
            return "The client needs to authenticate to gain network access. Intended for use by intercepting proxies used to control access to the network (e.g., \"captive portals\" used to require agreement to Terms of Service before granting full Internet access via a Wi-Fi hotspot).";
            
        default:
            return "Unknown Status Code!";
    }
}
?>
